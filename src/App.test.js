import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';

configure({ adapter: new Adapter() });

describe('App', () => {
    it('renders navbar', () => {
        const wrapper= shallow(<App />);
        expect(wrapper.find('Navbar'));
    })

    it('renders routes', () => {
        const wrapper= shallow(<App />);
        expect(wrapper.find('Routes'));
    })
});

import React from 'react';
import './App.css';
import Routes from './Routes.js'
// import Navbar from './components/jsx/NavigationBar.jsx';

function App() {

  return (
        <div className="App">
          {/* <Navbar /> */}
          <Routes />
        </div>
  );
}

export default App;

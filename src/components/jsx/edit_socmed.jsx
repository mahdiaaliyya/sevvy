import React, { Component } from 'react';
import 'antd/dist/antd.css';
import '../css/view_content.css';
import { Form, Input, Button, Space, Row, Col} from 'antd';
import { MinusCircleOutlined, PlusOutlined, SaveOutlined } from '@ant-design/icons';
import axios from 'axios';
import history from '../../history.js';

class Socmed extends Component {
    constructor(props){
        super(props);
        this.state = {
            situs : []
        }
    }

    componentDidMount(){
        axios.get("https://sevvydb.herokuapp.com/database/profile/situs/all", {
            params: {
                email: localStorage.getItem('email')
            }
        })
        .then(response => response.data)
        .then((data) => {
            this.setState({situs: data})
        });
    }

    handleRemove(link, id){
        const newSitus = [];
        for(var i=0; i <= this.state.situs.length-1; i++){
            if(this.state.situs[i].link !== link){
                newSitus.push(this.state.situs[i])
            }
        }
        this.setState({
            situs: newSitus
        });

        axios.delete("https://sevvydb.herokuapp.com/database/profile/situs",{
            params: {
                id: id
            }
        }).then(res => res.data)
    };

    render() {
        const onFinish = values => {
            const a = values.situs.length;
        
            for (var i=0; i<a; i++){
              const label = values.situs[i].label;
              const link = values.situs[i].link;
              axios.post("https://sevvydb.herokuapp.com/database/profile/situs/"+ localStorage.getItem('email'),
                    {
                        label: label,
                        link: link
                    },
                    { withCredentials: true }
                )
                .then(response => 
                        console.log(response.data)
                    )
            }
        };

        return ( 
            <div>
                { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && localStorage.getItem('email') !== null ? (
                    <div>
                        <h2 className="title">Situs Jaringan & Sosial Daring</h2> 
                    <div>
                        <div className="inputArea">
                        <Row>
                            <Col span={8}>
                            <div className="textArea">
                                LABEL
                            </div>
                            </Col>
                            <Col span={9}>
                            <div className="textArea">
                                LINK
                            </div>
                            </Col>
                        </Row>
                        {
                            this.state.situs.map((situs) => (
                            <Row>
                                <Col span={8}>
                                <div className="textField" key={situs.label}>
                                    {situs.label}
                                </div>
                                </Col>
                                <Col span={9}>
                                <div className="data" key={situs.link}>
                                    {situs.link}
                                </div>
                                </Col>
                                <Col span={4}>
                                    <MinusCircleOutlined onClick={(e) => this.handleRemove(situs.link, situs.id)}/>
                                </Col>
                            </Row>
                            ))
                        }
                        </div>           
                    </div>
                    <Form name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
                        <Form.List name="situs">
                        {(fields, { add, remove }) => {
                            return (
                            <div>
                                
                                {fields.map(field => (
                                <Space key={field.key} style={{ display: 'flex', marginBottom: 5 }} align="start">
                                    <Row>
                                    <Col >
                                        <Form.Item
                                        {...field}
                                        name={[field.name, 'label']}
                                        fieldKey={[field.fieldKey, 'label']}
                                        >
                                        <Input style={{width: '280px', marginRight: '46px'}} />
                                        </Form.Item>
                                    </Col>
                                    <Col >
                                        <Form.Item
                                        {...field}
                                        name={[field.name, 'link']}
                                        fieldKey={[field.fieldKey, 'link']}
                                        
                                        >
                                        <Input style={{width: '280px', marginRight: '10px'}}/>
                                        </Form.Item>
                                    </Col>

                                    {fields.length > 0 ? (
                                        <MinusCircleOutlined
                                        className="dynamic-delete-button"
                                        style={{ margin: '0 8px', marginTop:'8px'}}
                                        onClick={() => {
                                            remove(field.name);
                                        }}
                                        />
                                    ) : null}
                                    </Row>
                                </Space>
                                ))}

                                <Form.Item>
                                <Button
                                    type="dashed"
                                    onClick={() => {
                                    add();
                                    }}
                                    style={{ width: '20%' }}
                                >
                                    <PlusOutlined /> Tambah Situs
                                </Button>
                                </Form.Item>
                                <Form.Item>
                                <Button type="primary" icon={<SaveOutlined />} size={"large"} className="save" htmlType="submit">
                                    Simpan
                                </Button>
                                </Form.Item>
                            </div>
                            );
                        }}
                        </Form.List>
                    </Form>
                    </div>
                ) : history.push('/') }
            </div>
        );
    }
}
 
export default Socmed;
import React, { Component } from 'react';
import 'antd/dist/antd.css';
import '../css/view_content.css';
import { Row, Col, Tooltip } from 'antd';
import { EditFilled } from '@ant-design/icons';
import history from '../../history';
import axios from 'axios';

class ProfileInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            profile : []
        };
    }

    componentDidMount(){
        axios.get("https://sevvydb.herokuapp.com/database/profile/personal", {
            params: {
                email: localStorage.getItem('email')
            }
        })
        .then(response => response.data)
        .then((data) => {
            this.setState({profile: data})
        });
    }

    render() {
        return ( 
            <div>
                { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && localStorage.getItem('email') !== null ? (
                    <div>
                        <h2 className="title">Informasi Diri 
                            <Tooltip title="Ubah profil"  placement="right">
                                <EditFilled className="icn" onClick={() => history.push('/ubah-profil')}/>
                            </Tooltip>
                        </h2>
                        <div className="inputArea">
                            <Row>
                                <Col span={8}>
                                    <div className="textField">
                                        NAMA DEPAN
                                    </div>
                                    {
                                        this.state.profile.map((profile) => (
                                            <div className="data">
                                                {profile.firstName}
                                            </div>
                                        ))
                                    }
                                </Col>
                                <Col span={8}>
                                    <div className="textField">
                                        NAMA BELAKANG
                                    </div>
                                    {
                                        this.state.profile.map((profile) => (
                                            <div className="data">
                                                {profile.lastName}
                                            </div>
                                        ))
                                    }
                                </Col>
                            </Row>
                            <br />
                            <Row>
                                <Col span={12}>
                                    <div className="textField">
                                        EMAIL
                                    </div>
                                    {
                                        this.state.profile.map((profile) => (
                                            <div className="data">
                                                {profile.email}
                                            </div>
                                        ))
                                    }
                            </Col>
                            </Row>
                            <br />
                            <Row>
                                <Col span={12}>
                                    <div className="textField">
                                        NOMOR TELEPON
                                    </div>
                                    {
                                        this.state.profile.map((profile) => (
                                            <div className="data">
                                                {profile.phoneNumber}
                                            </div>
                                        ))
                                    }

                            </Col>
                            </Row>
                            <br />
                            <Row>
                                <Col span={15}>
                                    <div className="textField">
                                        ALAMAT
                                    </div>
                                    {
                                        this.state.profile.map((profile) => (
                                            <div className="data">
                                                {profile.address} 
                                                <br/> {profile.city}, {profile.province}
                                                <br/> {profile.postalCode}
                                            </div>
                                        ))
                                    }
                            </Col>
                            </Row>
                            <br />
                        </div>
                    </div>
                ) : history.push('/') }
            </div>
        );
    }
}
 
export default ProfileInfo;
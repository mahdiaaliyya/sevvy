import React, { Component } from 'react';
import 'antd/dist/antd.css';
import '../css/view_content.css';
import { Row, Col, Tooltip, Card } from 'antd';
import { EditFilled } from '@ant-design/icons';
import history from '../../history';
import axios from 'axios';

class Experience extends Component {
    constructor(props){
        super(props);
        this.state = {
            experience : []
        }
    }

    componentDidMount(){
        axios.get("https://sevvydb.herokuapp.com/database/profile/pengalaman/all", {
            params: {
                email: localStorage.getItem('email')
            }
        })
        .then(response => response.data)
        .then((data) => {
            this.setState({experience: data})
        });
    }

    render() {
        return ( 
            <div>
                { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && localStorage.getItem('email') !== null ? (
                    <div>
                    <h2 className="title">Pengalaman Kerja
                        <Tooltip title="Ubah profil"  placement="right">
                            <EditFilled className="icn" onClick={() => history.push('/ubah-profil')}/>
                        </Tooltip>
                    </h2>
                    {
                        this.state.experience.map((experience) => (
                            <div className="inputArea">
                                <Card style={{
                                    width: 700,
                                    paddingLeft: 20,
                                    paddingTop: 10,
                                    paddingBottom: 10,
                                    paddingRight: 20
                                }}>
                                    <Row>
                                        <Col>
                                            <div className="textField">
                                                {experience.posisi}
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <div className="text-ach">
                                                {experience.perusahaan}
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <div className="text-ach">
                                                {experience.periodeMulai} - {experience.periodeSelesai}
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                    <Col>
                                            <div className="text-ach">
                                                {experience.deskripsi}
                                            </div>
                                        </Col>
                                    </Row>
                                </Card>
                            </div>
                        ))
                    }
                    </div>
                ) : history.push('/') }
            </div>
        );
    }
}
 
export default Experience;
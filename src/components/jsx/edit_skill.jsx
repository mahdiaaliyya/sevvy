import React, { Component } from 'react';
import 'antd/dist/antd.css';
import '../css/view_content.css';
import { Form, Input, Button, Space, Row, Col, Slider, Progress } from 'antd';
import { MinusCircleOutlined, PlusOutlined, SaveOutlined } from '@ant-design/icons';
import axios from 'axios';
import history from '../../history.js';

class Skill extends Component {
    constructor(props){
        super(props);
        this.state = {
            skill : []
        }
    }

    componentDidMount(){
        axios.get("https://sevvydb.herokuapp.com/database/profile/keterampilan/all", {
            params: {
                email: localStorage.getItem('email')
            }
        })
        .then(response => response.data)
        .then((data) => {
            this.setState({skill: data})
        });
    }

    handleRemove(id){
        const newKeterampilan = [];
        for(var i=0; i <= this.state.skill.length-1; i++){
            if(this.state.skill[i].id !== id){
                newKeterampilan.push(this.state.skill[i])
            }
        }
        this.setState({
            skill: newKeterampilan
        });

        axios.delete("https://sevvydb.herokuapp.com/database/profile/keterampilan",{
            params: {
                id: id
            }
        }).then(res => res.data)
    };

    render() {
        const onFinish = values => {
            const a = values.keterampilan.length;
        
            for (var i=0; i<a; i++){
              const namaKeterampilan = values.keterampilan[i].namaKeterampilan;
              const tingkatKeterampilan = values.keterampilan[i].tingkatKeterampilan;
              axios.post("https://sevvydb.herokuapp.com/database/profile/keterampilan/"+ localStorage.getItem('email'),
                    {
                        namaKeterampilan: namaKeterampilan,
                        tingkatKeterampilan: tingkatKeterampilan
                    },
                    { withCredentials: true }
                )
                .then(response => 
                        console.log(response.data)
              )
            }
        };

        return ( 
            <div>
                { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && localStorage.getItem('email') !== null ? (
                    <div>
                        <h2 className="title">Keterampilan</h2> 
                        <div>
                            <div className="inputArea">
                                <Row>
                                    <Col span={8}>
                                        <div className="textArea">
                                            KETERAMPILAN
                                        </div>
                                    </Col>
                                    <Col span={8}>
                                        <div className="textArea">
                                            TINGKAT KETERAMPILAN
                                        </div>
                                    </Col>
                                </Row>
                                {
                                    this.state.skill.map((skill) => (
                                        <Row>
                                            <Col span={9}>
                                                <div className="textField">
                                                    {skill.namaKeterampilan}
                                                </div>
                                            </Col>
                                            <Col span={9}>
                                                <div className="data">
                                                    <Progress percent={skill.tingkatKeterampilan} steps={5} strokeColor="#1890ff" />
                                                </div>
                                            </Col>
                                            <Col span={4}>
                                                <MinusCircleOutlined onClick={(e) => this.handleRemove(skill.id)}/>
                                            </Col>
                                        </Row>
                                    ))
                                }
                            </div>
                        </div>
                        <Form name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
                            <Form.List name="keterampilan">
                            {(fields, { add, remove }) => {
                                return (
                                <div>
                                    {fields.map(field => (
                                    <Space key={field.key} style={{ display: 'flex', marginBottom: 5 }} align="start">
                                        <Row>
                                        <Col >
                                            <Form.Item
                                            {...field}
                                            name={[field.name, 'namaKeterampilan']}
                                            fieldKey={[field.fieldKey, 'namaKeterampilan']}
                                            >
                                            <Input style={{width: '300px', marginRight: '46px'}} />
                                            </Form.Item>
                                        </Col>
                                        <Col >
                                            <Form.Item
                                            {...field}
                                            name={[field.name, 'tingkatKeterampilan']}
                                            fieldKey={[field.fieldKey, 'tingkatKeterampilan']}
                                            
                                            >
                                            <Slider 
                                                marks={{
                                                    0: '0',
                                                    20: '20',
                                                    40: '40',
                                                    60: '60',
                                                    80: '80',
                                                    100: '100'
                                                }} 
                                                style={{width: '280px', marginRight: '10px'}}/>
                                            </Form.Item>
                                        </Col>

                                        {fields.length > 0 ? (
                                            <MinusCircleOutlined
                                            className="dynamic-delete-button"
                                            style={{ margin: '5px 20px', marginTop:'10px'}}
                                            onClick={() => {
                                                remove(field.name);
                                            }}
                                            />
                                        ) : null}
                                        </Row>
                                    </Space>
                                    ))}

                                    <Form.Item>
                                    <Button
                                        type="dashed"
                                        onClick={() => {
                                        add();
                                        }}
                                        style={{ width: '20%' }}
                                    >
                                        <PlusOutlined /> Tambah keterampilan
                                    </Button>

                                    </Form.Item>
                                    <Form.Item>
                                    <Button type="primary" icon={<SaveOutlined />} size={"large"} className="save" htmlType="submit">
                                        Simpan
                                    </Button>
                                    </Form.Item>
                                </div>
                                );
                            }}
                            </Form.List>
                        </Form>
                    </div>
                ) : history.push('/') }
            </div>
        );
    }
}
 
export default Skill;
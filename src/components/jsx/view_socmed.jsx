import React, { Component } from 'react';
import 'antd/dist/antd.css';
import '../css/view_content.css';
import { Row, Col, Tooltip } from 'antd';
import { EditFilled } from '@ant-design/icons';
import history from '../../history';
import axios from 'axios';

class Socmed extends Component {
    constructor(props){
        super(props);
        this.state = {
            situs : []
        }
    }

    componentDidMount(){
        axios.get("https://sevvydb.herokuapp.com/database/profile/situs/all", {
            params: {
                email: localStorage.getItem('email')
            }
        })
        .then(response => response.data)
        .then((data) => {
            this.setState({situs: data})
        });
    }

    render() {
        return ( 
            <div>
                { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && localStorage.getItem('email') !== null ? (
                    <div>
                        <h2 className="title">Situs Jaringan & Sosial Daring
                            <Tooltip title="Ubah profil"  placement="right">
                                <EditFilled className="icn" onClick={() => history.push('/ubah-profil')}/>
                            </Tooltip>
                        </h2> 
                        <div className="inputArea">
                            <Row>
                                <Col span={4}>
                                {
                                    this.state.situs.map((situs) => (
                                        <div className="textField" key={situs.label}>
                                            {situs.label}
                                        </div>
                                    ))
                                }
                                </Col>
                                <Col span={9}>
                                {
                                    this.state.situs.map((situs) => (
                                        <div className="data" key={situs.link}>
                                            {situs.link}
                                        </div>
                                    ))
                                }
                                </Col>
                            </Row>
                        </div>
                    </div>
                ) : history.push('/')}
            </div>
        );
    }
}
 
export default Socmed;
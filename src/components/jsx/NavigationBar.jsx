import React, {Component} from 'react';
import { Layout, Menu, Row, Col } from 'antd';
import 'antd/dist/antd.css';
import './../css/NavigationBar.css';
import history from '../../history';

import brandLogo from "../../images/brandLogo.png";

const RowStyle = {lineHeight:"63px"}

const MenuStyle = {float:'right'}

class NavigationBar extends Component {
    handleLogout() {
        localStorage.clear();
        history.push('/')
    }

    render() { 
        return (
            <Layout.Header>
                
                <Row style={RowStyle}>
                    <Col xs={20} sm={20} md={6} lg={6} xl={5} xxl={4} className="menu">
                        <a href="/" style={{display: 'flex', alignItems: 'center'}}>
                            <img src={brandLogo} alt="Sevvy" style={{height: '42px'}}/>
                            <h1 className="logo" style={{margin: '0px'}}>sevvy</h1></a>
                    </Col>
                    <Col xs={0} sm={0} md={18} lg={18} xl={19} xxl={20}>
                        <Menu mode="horizontal" style={MenuStyle} defaultSelectedKeys={['beranda']}>
                            <Menu.Item key="beranda" title="beranda" onClick={() => history.push('/dashboard')} >Beranda</Menu.Item>
                            <Menu.Item key="profil" title="profil" onClick={() => history.push('/profil')} >Profil Saya</Menu.Item>
                            <Menu.Item key="keluar" title="keluar" onClick={() => this.handleLogout()}>Keluar</Menu.Item>
                        </Menu>
                    </Col>
                </Row>
            </Layout.Header>
         );
    }
}
 
export default NavigationBar;
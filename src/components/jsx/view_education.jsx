import React, { Component } from 'react';
import 'antd/dist/antd.css';
import '../css/view_content.css';
import { Row, Col, Tooltip, Card } from 'antd';
import { EditFilled } from '@ant-design/icons';
import history from '../../history';
import axios from 'axios';

class Education extends Component {
    constructor(props){
        super(props);
        this.state = {
            education : []
        }
    }

    componentDidMount(){
        axios.get("https://sevvydb.herokuapp.com/database/profile/pendidikan/all", {
            params: {
                email: localStorage.getItem('email')
            }
        })
        .then(response => response.data)
        .then((data) => {
            this.setState({education: data})
        });
    }

    render() {
        return ( 
            <div>
                { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && localStorage.getItem('email') !== null ? (
                    <div>
                    <h2 className="title">Pendidikan
                        <Tooltip title="Ubah profil"  placement="right">
                            <EditFilled className="icn" onClick={() => history.push('/ubah-profil')}/>
                        </Tooltip>
                    </h2>     
                    {
                        this.state.education.map((education) => (
                            <div className="inputArea">
                                <Card style={{
                                    width: 700,
                                    paddingLeft: 20,
                                    paddingTop: 10,
                                    paddingBottom: 10,
                                    paddingRight: 20
                                }}>
                                    <Row>
                                        <Col span={9}>
                                            <div className="textField">
                                                {education.jenjang}, {education.namaInstitusi}
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={9}>
                                            <div className="text-ach">
                                                {education.jurusan}
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col span={9}>
                                            <div className="text-ach">
                                                {education.tahunMulai} - {education.tahunSelesai}
                                            </div>
                                        </Col>
                                    </Row>
                                </Card>
                            </div>
                        ))
                    }
                    </div>
                ) : history.push('/') }
            </div>
        );
    }
}
 
export default Education;
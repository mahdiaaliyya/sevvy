import React from 'react';
import 'antd/dist/antd.css';
import '../css/content.css';
import { Form, Input, Button, Space, Row, Col, Card } from 'antd';
import { DeleteFilled, PlusOutlined, SaveOutlined } from '@ant-design/icons';
import axios from 'axios';
import history from '../../history.js';

const { TextArea } = Input;

const Volunteer = () => {
    const onFinish = values => {
        const a = values.kegiatansukarela.length;

        for (var i=0; i<a; i++){
            const posisi = values.kegiatansukarela[i].posisi;
            const perusahaan = values.kegiatansukarela[i].perusahaan;
            const tahunMulai = values.kegiatansukarela[i].tahunMulai;
            const tahunSelesai = values.kegiatansukarela[i].tahunSelesai;
            const deskripsi = values.kegiatansukarela[i].deskripsi;

            axios.post("https://sevvydb.herokuapp.com/database/profile/kegiatan-sukarela/"+ localStorage.getItem('email'),
                    {
                        posisi: posisi,
                        perusahaan: perusahaan,
                        tahunMulai: tahunMulai,
                        tahunSelesai: tahunSelesai,
                        deskripsi: deskripsi
                    },
                    { withCredentials: true }
                )
                .then(response => 
                        console.log(response.data)
            )
        }
    };

  return (
      <div>
          { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && localStorage.getItem('email') !== null ? (
            <div>
            <h2>Kegiatan Sukarela</h2>
            <Form name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
                <Form.List name="kegiatansukarela">
                {(fields, { add, remove }) => {
                    return (
                        <div>
                            {fields.map(field => (
                            <Space key={field.key} style={{ display: 'flex', marginBottom: 5 }} align="start">
                                <div className="inputArea">
                                    <Row>
                                        <Col>
                                            <Card style={{paddingLeft: 30, paddingTop: 30, marginBottom: 10}}>
                                                <Row>
                                                    <Col span={10}>
                                                        <div className="textArea">
                                                            POSISI
                                                        </div>
                                                        <Form.Item
                                                        {...field}
                                                        name={[field.name, 'posisi']}
                                                        fieldKey={[field.fieldKey, 'posisi']}
                                                        >
                                                        <Input style={{width: '300px', marginRight: '46px'}} />
                                                        </Form.Item>
                                                    </Col>
                                                    <Col span={10}>
                                                        <div className="textArea" style={{marginLeft: '52px'}}>
                                                            PERUSAHAAN
                                                        </div>
                                                        <Form.Item
                                                        {...field}
                                                        name={[field.name, 'perusahaan']}
                                                        fieldKey={[field.fieldKey, 'perusahaan']}
                                                        >
                                                        <Input style={{width: '350px', marginLeft: '52px'}} />
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col >
                                                        <div className="textArea">
                                                            TAHUN MULAI
                                                        </div>
                                                        <Form.Item
                                                        {...field}
                                                        name={[field.name, 'tahunMulai']}
                                                        fieldKey={[field.fieldKey, 'tahunMulai']}
                                                        >
                                                        <Input  style={{width: '130px', marginRight: '46px'}} />
                                                        </Form.Item>
                                                    </Col>
                                                    <Col >
                                                        <div className="textArea">
                                                            TAHUN SELESAI
                                                        </div>
                                                        <Form.Item
                                                        {...field}
                                                        name={[field.name, 'tahunSelesai']}
                                                        fieldKey={[field.fieldKey, 'tahunSelesai']}
                                                        >
                                                        <Input  style={{width: '130px', marginRight: '46px'}} />
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col >
                                                        <div className="textArea">
                                                            DESKRIPSI
                                                        </div>
                                                            <Form.Item
                                                            {...field}
                                                            name={[field.name, 'deskripsi']}
                                                            fieldKey={[field.fieldKey, 'deskripsi']}
                                                        >
                                                            <TextArea style={{width: '600px', marginRight: '46px'}} />
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </Card>
                                        </Col>
                                        <Col>
                                            {fields.length > 1 ? (
                                                <DeleteFilled
                                                className="delete"
                                                style={{ margin: '0 8px', marginTop:'8px'}}
                                                onClick={() => {
                                                    remove(field.name);
                                                }}
                                                />
                                            ) : null}
                                        </Col>
                                    </Row>
                                    </div>
                                </Space>
                                ))}
                                <Form.Item>
                                <Button
                                    type="dashed"
                                    onClick={() => {
                                    add();
                                    }}
                                    style={{ width: '40%' }}
                                >
                                    <PlusOutlined /> Tambah Kegiatan Sukarela
                                </Button>
                                </Form.Item>
                                <Form.Item>
                                <Button type="primary" icon={<SaveOutlined />} size={"large"} className="save" htmlType="submit">
                                    Simpan
                                </Button>
                                </Form.Item>
                            </div>
                    );
                }}
                </Form.List>
            </Form>
            </div>
          ) : history.push('/') }
    </div>
  );
};

export default Volunteer;

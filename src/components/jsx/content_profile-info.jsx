import React, { Component } from 'react';
import 'antd/dist/antd.css';
import '../css/content.css';
import {Input, Row, Col, Button} from 'antd';
import { SaveOutlined } from '@ant-design/icons';
import axios from 'axios';
import history from '../../history.js';

class ProfileInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            phoneNumber: '',
            address: '',
            city: '',
            province: '',
            state: '',
            postalCode: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({
          [event.target.name]: event.target.value
        });
    }

    handleSubmit(e){
        const { firstName, lastName, phoneNumber, address, city, province, state, postalCode } = this.state;
        axios.put("https://sevvydb.herokuapp.com/database/profile/personal", 
            {
                firstName: firstName,
                lastName: lastName,
                phoneNumber: phoneNumber,
                email: localStorage.getItem('email'),
                address: address,
                city: city,
                province: province,
                state: state,
                postalCode: postalCode,
            },
            { withCredentials: true }
        )
        .then(response => 
                console.log(response.data)
            )
    }

    render() {
        return ( 
            <div>
                { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && localStorage.getItem('email') !== null ? (
                    <div >
                    <h2>Informasi Diri</h2>
                        <div className="inputArea">
                            <Row>
                                <Col span={12}>
                                    <div className="textArea">
                                        NAMA DEPAN
                                    </div>
                                    <Input 
                                        type="text" 
                                        name="firstName" 
                                        value={this.state.firstName}
                                        onChange={e => this.handleChange(e)}/>
                                </Col>
                                <Col span={12}>
                                    <div className="textArea">
                                        NAMA BELAKANG
                                    </div>
                                    <Input 
                                        type="text" 
                                        name="lastName" 
                                        value={this.state.lastName}
                                        onChange={e => this.handleChange(e)}/>
                                </Col>
                            </Row>
                            <br />
                            <Row>
                                <Col span={12}>
                                    <div className="textArea">
                                        EMAIL
                                    </div>
                                    {this.props.email}
                                </Col>
                            </Row>
                            <br />
                            <Row>
                                <Col span={12}>
                                    <div className="textArea">
                                        NOMOR TELEPON
                                    </div>
                                    <Input 
                                        type="text" 
                                        name="phoneNumber" 
                                        value={this.state.phoneNumber}
                                        onChange={e => this.handleChange(e)} />
                                </Col>
                            </Row>
                            <br />
                            <Row>
                                <Col span={15}>
                                    <div className="textArea">
                                        ALAMAT
                                    </div>
                                    <Input 
                                        type="text" 
                                        name="address" 
                                        value={this.state.address}
                                        onChange={e => this.handleChange(e)} />
                                </Col>
                                <Col span={9}>
                                    <div className="textArea">
                                        KOTA
                                    </div>
                                    <Input 
                                        type="text" 
                                        name="city" 
                                        value={this.state.city}
                                        onChange={e => this.handleChange(e)} />
                                </Col>
                            </Row>
                            <br />
                            <Row>
                                <Col span={15}>
                                    <div className="textArea">
                                        PROVINSI
                                    </div>
                                    <Input 
                                        type="text" 
                                        name="province" 
                                        value={this.state.province}
                                        onChange={e => this.handleChange(e)}/>
                                </Col>
                                <Col span={9}>
                                    <div className="textArea">
                                        KODE POS
                                    </div>
                                    <Input 
                                        type="text" 
                                        name="postalCode" 
                                        value={this.state.postalCode}
                                        onChange={e => this.handleChange(e)} />
                                </Col>
                            </Row>
                            <br/>
                            <Row>
                                <Button type="primary" icon={<SaveOutlined />} size={"large"} className="save" onClick={(e) => this.handleSubmit(e)}>
                                    Simpan
                                </Button>
                            </Row>
                        </div>
                    </div>
                ) : history.push('/') }
            </div>
        );
    }
}
 
export default ProfileInfo;
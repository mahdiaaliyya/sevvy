import React from 'react';
import 'antd/dist/antd.css';
import '../css/content.css';
import { Form, Input, Button, Space, Row, Col, Slider } from 'antd';
import { MinusCircleOutlined, PlusOutlined, SaveOutlined } from '@ant-design/icons';
import axios from 'axios';
import history from '../../history.js';

const Skill = () => {
  const onFinish = values => {
    const a = values.keterampilan.length;

    for (var i=0; i<a; i++){
      const namaKeterampilan = values.keterampilan[i].namaKeterampilan;
      const tingkatKeterampilan = values.keterampilan[i].tingkatKeterampilan;
      axios.post("https://sevvydb.herokuapp.com/database/profile/keterampilan/"+ localStorage.getItem('email'),
            {
                namaKeterampilan: namaKeterampilan,
                tingkatKeterampilan: tingkatKeterampilan
            },
            { withCredentials: true }
        )
        .then(response => 
                console.log(response.data)
      )
    }
  };

  return (
    <div>
      { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && localStorage.getItem('email') !== null ? (
        <div>
          <h2>Keterampilan</h2>
          <Form name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
            <Form.List name="keterampilan">
              {(fields, { add, remove }) => {
                return (
                  <div>
                    <Row>
                      <Col span={8}>
                      <div className="textArea">
                          NAMA KETERAMPILAN
                        </div>
                      </Col>
                      <Col span={8}>
                      <div className="textArea" style={{marginLeft: '20px'}}>
                          TINGKAT KETERAMPILAN
                        </div>
                      </Col>
                    </Row>
                    {fields.map(field => (
                      <Space key={field.key} style={{ display: 'flex', marginBottom: 5 }} align="start">
                        <Row>
                          <Col >
                            <Form.Item
                              {...field}
                              name={[field.name, 'namaKeterampilan']}
                              fieldKey={[field.fieldKey, 'namaKeterampilan']}
                            >
                              <Input style={{width: '300px', marginRight: '46px'}} />
                            </Form.Item>
                          </Col>
                          <Col >
                            <Form.Item
                              {...field}
                              name={[field.name, 'tingkatKeterampilan']}
                              fieldKey={[field.fieldKey, 'tingkatKeterampilan']}
                              
                            >
                              <Slider 
                                marks={{
                                    0: '0',
                                    20: '20',
                                    40: '40',
                                    60: '60',
                                    80: '80',
                                    100: '100'
                                }} 
                                style={{width: '280px', marginRight: '10px'}}/>
                            </Form.Item>
                          </Col>

                          {fields.length > 1 ? (
                            <MinusCircleOutlined
                              className="dynamic-delete-button"
                              style={{ margin: '5px 20px', marginTop:'10px'}}
                              onClick={() => {
                                remove(field.name);
                              }}
                            />
                          ) : null}
                        </Row>
                      </Space>
                    ))}

                    <Form.Item>
                      <Button
                        type="dashed"
                        onClick={() => {
                          add();
                        }}
                        style={{ width: '20%' }}
                      >
                        <PlusOutlined /> Tambah keterampilan
                      </Button>

                    </Form.Item>
                    <Form.Item>
                      <Button type="primary" icon={<SaveOutlined />} size={"large"} className="save" htmlType="submit">
                        Simpan
                      </Button>
                    </Form.Item>
                  </div>
                );
              }}
            </Form.List>
          </Form>
        </div>
      ) : history.push('/') }
    </div>
  );
};

export default Skill;
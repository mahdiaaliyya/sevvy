import React, {Component} from 'react';
import { Layout } from 'antd';
import 'antd/dist/antd.css';

const { Footer } = Layout;

class FooterLayout extends Component {
    render() { 
        return (
            <Layout>
                <Footer style={{ textAlign: 'center' }}>Sevvy © 2020 Created by Kelompok 11</Footer>
            </Layout>
         );
    }
}
 
export default FooterLayout;
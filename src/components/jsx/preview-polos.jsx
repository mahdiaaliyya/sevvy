import React, { Component } from 'react';
import { Row, Col } from 'antd';
import axios from 'axios';
import history from '../../history.js';

class PreviewPolos extends Component{
    constructor(props){
        super(props);
        this.state = {
            tmplt: false,
            title: '#262527',
            template: 'PLAIN',
            colorCode: 1,
            cvName: 'your_cv',
            lang: localStorage.getItem('checked'), 
            profile : [],
            education: [],
            skill: [],
            socmed: [],
            experience: [],
            volunteer: [],
            achievement: []
        };
    }

    componentDidMount(){
        axios
            .get("https://sevvydb.herokuapp.com/database/profile/personal", {
                params: {
                    email: localStorage.getItem('email')
                }
            })
            .then(response => {
                this.setState({profile: response.data});
                return axios.get("https://sevvydb.herokuapp.com/database/profile/pendidikan/all", {
                    params: {
                        email: localStorage.getItem('email')
                    }
                });
            })
            .then(response => {
                this.setState({education: response.data});
                return axios.get("https://sevvydb.herokuapp.com/database/profile/keterampilan/all", {
                    params: {
                        email: localStorage.getItem('email')
                    }
                });
            })
            .then(response => {
                this.setState({skill: response.data});
                return axios.get("https://sevvydb.herokuapp.com/database/profile/situs/all", {
                    params: {
                        email: localStorage.getItem('email')
                    }
                });
            })
            .then(response => {
                this.setState({socmed: response.data});
                return axios.get("https://sevvydb.herokuapp.com/database/profile/pengalaman/all", {
                    params: {
                        email: localStorage.getItem('email')
                    }
                });
            })
            .then(response => {
                this.setState({experience: response.data});
                return axios.get("https://sevvydb.herokuapp.com/database/profile/kegiatan-sukarela/all", {
                    params: {
                        email: localStorage.getItem('email')
                    }
                });
            })
            .then(response => {
                this.setState({volunteer: response.data});
                return axios.get("https://sevvydb.herokuapp.com/database/profile/pencapaian/all", {
                    params: {
                        email: localStorage.getItem('email')
                    }
                })
            })
            .then(response => {
                this.setState({achievement: response.data});
            });
    }

    render(){
        var bg=require('../../images/polos.png');
        return (    
        <div>
            { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && localStorage.getItem('email') !== null ? (
                <div style={{backgroundImage: "url("+bg+")", height: '1113.56px'}}>
                    <Col span={24} style={{
                        paddingLeft: '63px',
                        fontFamily: '"Times New Roman", Georgia, Serif',
                    }}>
                    {
                        this.state.profile.map((p) => (
                            <div>
                                <Row style={{
                                    marginTop: '40px',
                                    fontSize: '34px',
                                    color: localStorage.getItem('title'),
                                    paddingTop: '30px'
                                }}>
                                    <div style={{fontWeight: 'bold'}}>{p.firstName}</div>
                                    <div style={{marginLeft: '2%'}}>{p.lastName}</div>
                                </Row>
                                <Row style={{
                                    fontSize: '16px',
                                    color: '#262527'
                                }}>
                                    <div>{p.email} | {p.phoneNumber} | {p.address}, {p.city}, {p.province}, {p.postalCode}</div>
                                </Row>
                            </div>
                        ))
                    }
                        <Row style={{marginTop: '11.5px'}}>
                            <Col span={6} >
                                {
                                    this.state.socmed.map((socmed) => (
                                        <div style={{fontSize: '16px'}}>
                                            {socmed.label}
                                        </div>
                                    ))
                                }
                            </Col>
                            <Col span={13}>
                                {
                                    this.state.socmed.map((socmed) => (
                                        <div style={{fontSize: '16px'}}>
                                            {socmed.link}
                                        </div>
                                    ))
                                }
                            </Col>
                        </Row>
                        {/* experience */}
                        <Row style={{marginTop: '8px'}}>
                            <Col span={24} style={{
                                fontSize: '20px',
                                color: localStorage.getItem('title'),
                                fontWeight: 'bold'
                            }}>
                                {localStorage.getItem('work')}
                                
                            </Col>
                            <Col span={24} >
                                {
                                    this.state.experience.map((experience) => (
                                        <div style={{paddingBottom: '1%', marginLeft: '20px', paddingRight: '43.7px', fontSize: '16px'}}>
                                            <div style={{fontWeight: 'bold'}}>{experience.posisi} ({experience.periodeMulai} - {experience.periodeSelesai})</div>
                                            {experience.perusahaan}
                                            <br/>{experience.deskripsi}
                                        </div>
                                    ))
                                }
                            </Col>
                        </Row>
                        {/* education */}
                        <Row style={{marginTop: '8px'}}>
                            <Col span={24} style={{
                                fontSize: '20px',
                                color: localStorage.getItem('title'),
                                fontWeight: 'bold'
                            }}>
                                {localStorage.getItem('edu')}
                            </Col>
                            <Col span={24} >
                                {
                                    this.state.education.map((education) => (
                                        <div style={{paddingBottom: '1%', marginLeft: '20px', paddingRight: '43.7px', fontSize: '16px'}}>
                                            <div style={{fontWeight: 'bold'}}>{education.jenjang} of {education.jurusan}</div>
                                            {education.namaInstitusi}
                                            <br/>{education.tahunMulai} - {education.tahunSelesai}
                                        </div>
                                    ))
                                }
                            </Col>
                        </Row>
                        {/* achievements */}
                        <Row style={{marginTop: '8px'}}>
                            <Col span={24} style={{
                                fontSize: '20px',
                                color: localStorage.getItem('title'),
                                fontWeight: 'bold'
                            }}>
                                {localStorage.getItem('ach')}
                            </Col>
                            <Col span={24} >
                                {
                                    this.state.achievement.map((achievement) => (
                                        <div style={{paddingBottom: '1%', marginLeft: '20px', paddingRight: '43.7px', fontSize: '16px'}}>
                                            <div style={{fontWeight: 'bold'}}>{achievement.title} ({achievement.tahun})</div>
                                            {achievement.institusi}
                                            <br/>{achievement.deskripsi}
                                        </div>
                                    ))
                                }
                            </Col>
                        </Row>
                        {/* volunteer */}
                        <Row style={{marginTop: '8px'}}>
                            <Col span={24} style={{
                                fontSize: '20px',
                                color: localStorage.getItem('title'),
                                fontWeight: 'bold'
                            }}>
                                {localStorage.getItem('vol')}
                            </Col>
                            <Col span={24} >
                                {
                                    this.state.volunteer.map((volunteer) => (
                                        <div style={{paddingBottom: '1%', marginLeft: '20px', paddingRight: '43.7px', fontSize: '16px'}}>
                                            <div style={{fontWeight: 'bold'}}>{volunteer.posisi} ({volunteer.tahunMulai} - {volunteer.tahunSelesai})</div>
                                            {volunteer.perusahaan}
                                            <br/>{volunteer.deskripsi}
                                        </div>
                                    ))
                                }
                            </Col>
                        </Row>
                        {/* skill */}
                        <Row style={{marginTop: '8px'}}>
                            <Col span={24} style={{
                                fontSize: '20px',
                                color: localStorage.getItem('title'),
                                fontWeight: 'bold'
                            }}>
                                {localStorage.getItem('skl')}
                            </Col>
                            <Col span={24} >
                                {
                                    this.state.skill.map((skill) => (
                                        <div style={{marginLeft: '20px', paddingRight: '43.7px', fontSize: '16px'}}>
                                            {skill.namaKeterampilan} ({skill.tingkatKeterampilan}%)
                                        </div>
                                    ))
                                }
                            </Col>
                        </Row>
                    </Col>
                </div>
            ) : history.push('/') }
        </div>
        )
    }
}

export default PreviewPolos;
import React, { Component } from 'react';
import 'antd/dist/antd.css';
import '../css/view_content.css';
import { Row, Col, Tooltip, Progress } from 'antd';
import { EditFilled } from '@ant-design/icons';
import history from '../../history';
import axios from 'axios';

class Skill extends Component {
    constructor(props){
        super(props);
        this.state = {
            skill : []
        }
    }

    componentDidMount(){
        axios.get("https://sevvydb.herokuapp.com/database/profile/keterampilan/all", {
            params: {
                email: localStorage.getItem('email')
            }
        })
        .then(response => response.data)
        .then((data) => {
            this.setState({skill: data})
        });
    }

    render() {
        return ( 
            <div>
                { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && localStorage.getItem('email') !== null ? (
                    <div>
                        <h2 className="title">Keterampilan
                            <Tooltip title="Ubah profil"  placement="right">
                                <EditFilled className="icn" onClick={() => history.push('/ubah-profil')}/>
                            </Tooltip>
                        </h2> 
                        <div className="inputArea">
                            {
                                this.state.skill.map((skill) => (
                                    <Row>
                                        <Col span={9}>
                                            <div className="textField">
                                                {skill.namaKeterampilan}
                                            </div>
                                        </Col>
                                        <Col span={9}>
                                            <div className="data">
                                                <Progress percent={skill.tingkatKeterampilan} steps={5} strokeColor="#1890ff" />
                                            </div>
                                        </Col>
                                    </Row>
                                    ))
                                }
                        </div>
                    </div>
                ) : history.push('/')}
            </div>
        );
    }
}
 
export default Skill;
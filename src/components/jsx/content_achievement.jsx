import React from 'react';
import 'antd/dist/antd.css';
import '../css/content.css';
import { Form, Input, Button, Space, Row, Col, Card } from 'antd';
import { DeleteFilled, PlusOutlined, SaveOutlined } from '@ant-design/icons';
import axios from 'axios';
import history from '../../history.js';

const { TextArea } = Input;

const Achievement = () => {
    const onFinish = values => {
        const a = values.pencapaian.length;

        for (var i=0; i<a; i++){
            const title = values.pencapaian[i].title;
            const institusi = values.pencapaian[i].institusi;
            const tahun = values.pencapaian[i].tahun;
            const deskripsi = values.pencapaian[i].deskripsi;

            axios.post("https://sevvydb.herokuapp.com/database/profile/pencapaian/"+ localStorage.getItem('email'),
                    {
                        title: title,
                        institusi: institusi,
                        tahun: tahun,
                        deskripsi: deskripsi
                    },
                    { withCredentials: true }
                )
                .then(response => 
                        console.log(response.data)
            )
        }
    };

  return (
      <div>
          { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && localStorage.getItem('email') !== null ? (
            <div>
                <h2>Pencapaian</h2>
                <Form name="dynamic_form_nest_item" onFinish={onFinish} autoComplete="off">
                    <Form.List name="pencapaian">
                    {(fields, { add, remove }) => {
                        return (
                            <div>
                                {fields.map(field => (
                                <Space key={field.key} style={{ display: 'flex', marginBottom: 5 }} align="start">
                                    <div className="inputArea">
                                        <Row>
                                            <Col>
                                                <Card style={{paddingLeft: 30, paddingTop: 30, marginBottom: 10}}>
                                                    <Row>
                                                        <Col span={10}>
                                                            <div className="textArea">
                                                                TITLE
                                                            </div>
                                                            <Form.Item
                                                            {...field}
                                                            name={[field.name, 'title']}
                                                            fieldKey={[field.fieldKey, 'title']}
                                                            >
                                                            <Input style={{width: '300px', marginRight: '46px'}} />
                                                            </Form.Item>
                                                        </Col>
                                                        <Col span={10}>
                                                            <div className="textArea">
                                                                INSTITUSI
                                                            </div>
                                                            <Form.Item
                                                            {...field}
                                                            name={[field.name, 'institusi']}
                                                            fieldKey={[field.fieldKey, 'institusi']}
                                                            >
                                                            <Input style={{width: '350px', marginRight: '46px'}} />
                                                            </Form.Item>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col >
                                                            <div className="textArea">
                                                                TAHUN
                                                            </div>
                                                            <Form.Item
                                                            {...field}
                                                            name={[field.name, 'tahun']}
                                                            fieldKey={[field.fieldKey, 'tahun']}
                                                            >
                                                            <Input  style={{width: '130px', marginRight: '46px'}} />
                                                            </Form.Item>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col >
                                                            <div className="textArea">
                                                                DESKRIPSI
                                                            </div>
                                                                <Form.Item
                                                                {...field}
                                                                name={[field.name, 'deskripsi']}
                                                                fieldKey={[field.fieldKey, 'deskripsi']}
                                                            >
                                                                <TextArea style={{width: '600px', marginRight: '46px'}} />
                                                            </Form.Item>
                                                        </Col>
                                                    </Row>
                                                </Card>
                                            </Col>
                                            <Col>
                                                {fields.length > 1 ? (
                                                    <DeleteFilled
                                                    className="delete"
                                                    title="delete"
                                                    style={{ margin: '0 8px', marginTop:'8px'}}
                                                    onClick={() => {
                                                        remove(field.name);
                                                    }}
                                                    />
                                                ) : null}
                                            </Col>
                                        </Row>
                                        </div>
                                    </Space>
                                    ))}
                                    <Form.Item>
                                    
                                    <Button
                                        type="dashed"
                                        onClick={() => {add()}}
                                        style={{ width: '40%' }}
                                    >
                                        <PlusOutlined /> Tambah Pencapaian
                                    </Button>

                                    </Form.Item>
                                    <Form.Item>
                                    <Button type="primary" icon={<SaveOutlined />} size={"large"} className="save" htmlType="submit">
                                        Simpan
                                    </Button>
                                    </Form.Item>
                                </div>
                        );
                    }}
                    </Form.List>
                </Form>
            </div>
        ) : history.push('/') }
    </div>
  );
};

export default Achievement;

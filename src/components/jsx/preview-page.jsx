import React, { Component } from 'react';
import { Row, Col, Layout, Progress, Button, Modal, Input, Checkbox, Form } from 'antd';
import { FilePdfOutlined, SaveOutlined } from '@ant-design/icons';
import axios from 'axios';
import * as FileSaver from "file-saver"; 

import Polos from './preview-polos.jsx';

class Preview extends Component {
    constructor(props){
        super(props);
        this.state = {
            atas: '#4c4a55',
            samping: '#FBFEFB',
            base: '#fff',
            title: '#262527',
            tmplt: true,
            template: 'MODERN',
            colorCode: 1,
            cvName: '',
            lang: false,
            cvLang: 'ENG',
            personalInfo: 'Personal Info',
            edu: 'Education',
            skills: 'Skills',
            socialMedia: 'Social Media',
            work: 'Work Experience',
            vol: 'Volunteer',
            ach: 'Achievements',
            profile : [],
            education: [],
            skill: [],
            socmed: [],
            experience: [],
            volunteer: [],
            achievement: []
        };
    }

    componentDidMount(){
        axios
            .get("https://sevvydb.herokuapp.com/database/profile/personal", {
                params: {
                    email: localStorage.getItem('email')
                }
            })
            .then(response => {
                this.setState({profile: response.data});
                return axios.get("https://sevvydb.herokuapp.com/database/profile/pendidikan/all", {
                    params: {
                        email: localStorage.getItem('email')
                    }
                });
            })
            .then(response => {
                this.setState({education: response.data});
                return axios.get("https://sevvydb.herokuapp.com/database/profile/keterampilan/all", {
                    params: {
                        email: localStorage.getItem('email')
                    }
                });
            })
            .then(response => {
                this.setState({skill: response.data});
                return axios.get("https://sevvydb.herokuapp.com/database/profile/situs/all", {
                    params: {
                        email: localStorage.getItem('email')
                    }
                });
            })
            .then(response => {
                this.setState({socmed: response.data});
                return axios.get("https://sevvydb.herokuapp.com/database/profile/pengalaman/all", {
                    params: {
                        email: localStorage.getItem('email')
                    }
                });
            })
            .then(response => {
                this.setState({experience: response.data});
                return axios.get("https://sevvydb.herokuapp.com/database/profile/kegiatan-sukarela/all", {
                    params: {
                        email: localStorage.getItem('email')
                    }
                });
            })
            .then(response => {
                this.setState({volunteer: response.data});
                return axios.get("https://sevvydb.herokuapp.com/database/profile/pencapaian/all", {
                    params: {
                        email: localStorage.getItem('email')
                    }
                })
            })
            .then(response => {
                this.setState({achievement: response.data});
            });
    }

    handleChange(event) {
        this.setState({
          [event.target.name]: event.target.value
        });
    }

    showModal = () => {
        this.setState({
          visible: true,
        });
    };

    handleOk = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
        this.downloadPDF();
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    downloadPDF(){
        if( this.state.tmplt === true) {
            axios.get("https://sevvydb.herokuapp.com/database/pdf/modern", {
                params: {
                    email: localStorage.getItem('email'),
                    colorCode: this.state.colorCode,
                    cvName: this.state.cvName,
                    cvLang: this.state.cvLang,
                },
                responseType: 'arraybuffer',
            }).then(response => {
                console.log(response);
                const blob = new Blob([response.data], {
                type: 'application/pdf',
                });
                FileSaver.saveAs(blob, this.state.cvName+(".pdf"));
            });
        } else if( this.state.tmplt === false){
            axios.get("https://sevvydb.herokuapp.com/database/pdf/formal", {
                params: {
                    email: localStorage.getItem('email'),
                    colorCode: this.state.colorCode,
                    cvName: this.state.cvName,
                    cvLang: this.state.cvLang,
                },
                responseType: 'arraybuffer',
            }).then(response => {
                console.log(response);
                const blob = new Blob([response.data], {
                type: 'application/pdf',
                });
                FileSaver.saveAs(blob, this.state.cvName+(".pdf"));
            });
        }
    }

    template1 = (e) => {
        this.setState({
            template: 'MODERN',
            atas: '#4c4a55',
            samping: '#FBFEFB',
            base: '#fff',
            tmplt: true,
            colorCode: 1
        })
    }
    
    template2 = (e) => {
        this.setState({
            template: 'MODERN',
            atas: '#2D3047',
            samping: '#FAC9B8',
            base: '#FBF5F3',
            tmplt: true,
            colorCode: 2
        })
    }

    saveCV(){
        axios.post("https://sevvydb.herokuapp.com/database/cv/"+ localStorage.getItem('email'),
            {
                cvName: this.state.cvName,
                template: this.state.template,
                cvLang: this.state.cvLang
            },
            { withCredentials: true }
        )
        .then(response => 
            console.log(response.data)
      )
    }

    polos(e){
        this.setState({
            tmplt: false
        })
        if(e === '1'){
            localStorage.setItem('title', '#262527')
            this.setState({
                colorCode: 3
            })
        }else if (e === '2'){
            localStorage.setItem('title', '#DC352A')
            this.setState({
                colorCode: 4
            })
        }
    }

    onChange(e){
        const checked = e.target.checked;
        this.setState({
          lang: checked
        })
        this.checked(e)
    }

    checked(e){
        if(this.state.lang === false){
            this.setState({
                template: 'FORMAL',
                cvLang: 'IND',
                personalInfo: 'Informasi Diri',
                edu: 'Pendidikan',
                skills: 'Keterampilan',
                socialMedia: 'Situs Jaringan & Sosial Daring',
                work: 'Pengalaman Kerja',
                vol: 'Kegiatan Sukarela',
                ach: 'Pencapaian',
            })
            localStorage.setItem('work', 'Pengalaman Kerja')
            localStorage.setItem('edu', 'Pendidikan')
            localStorage.setItem('skl', 'Keterampilan')
            localStorage.setItem('vol', 'Situs Jaringan & Sosial Daring')
            localStorage.setItem('ach', 'Pencapaian')
        } else if (this.state.lang === true){
            this.setState({
                template: 'FORMAL',
                cvLang: 'ENG',
                personalInfo: 'Personal Info',
                edu: 'Education',
                skills: 'Skills',
                socialMedia: 'Social Media',
                work: 'Work Experience',
                vol: 'Volunteer',
                ach: 'Achievements',
            })
            localStorage.setItem('work', 'Work Experience')
            localStorage.setItem('edu', 'Education')
            localStorage.setItem('skl', 'Skills')
            localStorage.setItem('vol', 'Volunteer')
            localStorage.setItem('ach', 'Achievements')
        }
    }

    render() {

        return ( 
            <div>
                <Modal
                    title="Unduh CV"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <p>Anda akan mengunduh {this.state.cvName}</p>
                </Modal>
                <Row>
                    <Col span={4}>
                        <img src={require('../../images/template1.png')} alt='' style={{width: '37px', height: '37px', marginRight: '10px'}}/>
                        <Button type="primary" size={"large"} onClick={this.template1}>
                            Template 1
                        </Button>
                    </Col>
                    <Col span={4}>
                        <img src={require('../../images/template2.png')} alt='' style={{width: '37px', height: '37px', marginRight: '10px'}}/>
                        <Button type="primary" size={"large"} onClick={this.template2}>
                            Template 2
                        </Button>
                    </Col>
                    <Col span={7}>
                        <Button type="primary" size={"large"} onClick={()=>this.polos('1')} style={{marginRight: '10px'}}>
                            Plain
                        </Button>
                        <Button type="primary" size={"large"} onClick={()=>this.polos('2')} >
                            Plain 2
                        </Button>
                    </Col>
                    <Col span={9}>
                        <Checkbox onChange={(e) => this.onChange(e)} style={{marginRight: '50px', paddingRight: '50px'}}>Bahasa Indonesia</Checkbox>
                        <Button type="primary" icon={<FilePdfOutlined/>} size={"large"} onClick={() => this.showModal()}>
                            Unduh CV
                        </Button>
                    </Col>
                </Row>
                <Row style={{marginTop: '20px'}}>
                    <Col span={2} style={{marginTop: '5px'}}>
                        <div className="textArea">
                            Nama CV :
                        </div>
                    </Col>
                    <Col span={6} >
                        <Form.Item
                            name="cvName"
                            rules={[
                                {
                                    required: true,
                                    message: 'Nama CV harus diisi!',
                                },
                            ]}    
                        >
                            <Input 
                                type="text" 
                                name="cvName" 
                                value={this.state.cvName}
                                onChange={e => this.handleChange(e)}
                                required />
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                    </Col>
                    <Col span={4} style={{
                        paddingLeft: '21px'
                    }}>
                        <Button type="primary" icon={<SaveOutlined/>} size={"large"} onClick={() => this.saveCV()}>
                            Simpan CV
                        </Button>
                    </Col>
                </Row>
                <div style={{
                    backgroundColor: '#d3d3d3',
                    marginTop: '5%'
                    }}>
                    <Row>
                        <div style={{
                            width: '793.7px',
                            height: '1113.56px',
                            marginTop: '5%',
                            marginLeft: 'auto',
                            marginRight: 'auto',
                            marginBottom: '5%',
                            boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
                        }}>
                            <Layout style={{
                                height: '1113.56px'}}
                            >
                                {this.state.tmplt === true ? (
                                    <div>
                                    <Row>
                                        {console.log("nama cv " + this.state.cvName)}
                                        <Col span={24} style={{
                                            backgroundColor: this.state.atas,
                                            height: '136px',
                                            fontFamily: '"Times New Roman", Georgia, Serif',
                                            color: '#fff',
                                            paddingLeft: '50px'
                                        }}>
                                            
                                                {
                                                    this.state.profile.map((profile) => (
                                                        <div>
                                                            <Row style={{
                                                                marginTop: '18px',
                                                                fontSize: '38px',
                                                            }}>
                                                                <Col style={{
                                                                    fontWeight: 'bold',
                                                                }}>
                                                                    {profile.firstName}
                                                                </Col>
                                                                <Col style={{
                                                                    marginLeft: '2%'
                                                                }}>
                                                                    {profile.lastName}
                                                                </Col>
                                                            </Row>
                                                            <Row style={{
                                                                fontFamily: '"Times New Roman", Georgia, Serif',
                                                                fontSize: '20px',
                                                            }}>
                                                                {profile.email} | {profile.phoneNumber}
                                                            </Row>
                                                        </div>
                                                    ))
                                                }
                                            
                                        </Col>
                                    </Row>
                                        <Row>
                                            <Col span={11} style={{
                                                backgroundColor: this.state.samping,
                                                height: '978px',
                                                fontFamily: '"Times New Roman", Georgia, Serif',
                                                paddingLeft: '35px',
                                            }}>
                                                {
                                                    this.state.profile.map((profile) => (
                                                        <div>
                                                            <Row style={{
                                                                marginTop: '18px',
                                                                color: '#4c4a55',
                                                                fontSize: '20px',
                                                                fontWeight: 'bold',
                                                            }}>
                                                                {this.state.personalInfo}
                                                            </Row>
                                                            <Row style={{
                                                                fontSize: '16px',
                                                            }}>
                                                                {profile.address}, {profile.city}
                                                                <br/>{profile.province}
                                                                <br/>{profile.postalCode}
                                                            </Row>
                                                        </div>
                                                    ))
                                                }
                                                {/* education */}
                                                <Row style={{
                                                    marginTop: '28px',
                                                    color: '#4c4a55',
                                                    fontSize: '20px',
                                                    fontWeight: 'bold',
                                                }}>
                                                    {this.state.edu}
                                                </Row>
                                                <Row style={{
                                                    fontSize: '16px',
                                                }}>
                                                    {
                                                        this.state.education.map((education) => (
                                                            <div>
                                                                <div style={{fontWeight: 'bold'}}>{education.jenjang} of {education.jurusan}</div>
                                                                    {education.namaInstitusi}
                                                                <br/>{education.tahunMulai} - {education.tahunSelesai}
                                                            </div>
                                                        ))
                                                    }
                                                </Row>
                                                {/* skill */}
                                                <Row style={{
                                                    marginTop: '28px',
                                                    color: '#4c4a55',
                                                    fontSize: '20px',
                                                    fontWeight: 'bold',
                                                }}>
                                                    {this.state.skills}
                                                </Row>
                                                <Row style={{fontSize: '16px'}}>
                                                    {
                                                        this.state.skill.map((skill) => (
                                                            <div>
                                                                <Col>
                                                                    {skill.namaKeterampilan} 
                                                                </Col>
                                                                <Col style={{marginBottom: '2%'}}>
                                                                    <Progress percent={skill.tingkatKeterampilan} steps={10} strokeColor="#4C4A55" /> 
                                                                </Col>
                                                            </div>
                                                        ))
                                                    }
                                                </Row>
                                                {/* Social media */}
                                                <Row style={{
                                                    marginTop: '28px',
                                                    color: '#4c4a55',
                                                    fontSize: '20px',
                                                    fontWeight: 'bold',
                                                }}>
                                                    {this.state.socialMedia}
                                                </Row>
                                                <Row>
                                                    <Col span={9} >
                                                        {
                                                            this.state.socmed.map((socmed) => (
                                                                <div style={{fontSize: '16px'}}>
                                                                    {socmed.label}
                                                                </div>
                                                            ))
                                                        }
                                                    </Col>
                                                    <Col span={13}>
                                                        {
                                                            this.state.socmed.map((socmed) => (
                                                                <div style={{fontSize: '16px'}}>
                                                                    {socmed.link}
                                                                </div>
                                                            ))
                                                        }
                                                    </Col>
                                                </Row>
                                            </Col>
                                            {/* experience */}
                                            <Col span={13} style={{
                                                backgroundColor: this.state.base,
                                                height: '978px',
                                                fontFamily: '"Times New Roman", Georgia, Serif',
                                                color: '#4c4a55',
                                                paddingLeft: '15px',
                                                paddingRight: '5%'
                                            }}>
                                                <Row style={{
                                                    marginTop: '18px',
                                                    color: '#4c4a55',
                                                    fontSize: '20px',
                                                    fontWeight: 'bold',
                                                }}>
                                                    {this.state.work}
                                                </Row>
                                                <Row style={{
                                                    fontSize: '16px', color: '#000'
                                                }}>
                                                    {
                                                        this.state.experience.map((experience) => (
                                                            <div style={{paddingBottom: '4%'}}>
                                                                <div style={{fontWeight: 'bold'}}>{experience.posisi} ({experience.periodeMulai} - {experience.periodeSelesai})</div>
                                                                {experience.perusahaan}
                                                                <br/>{experience.deskripsi}
                                                            </div>
                                                        ))
                                                    }
                                                </Row>
                                                {/* volunteer */}
                                                <Row style={{
                                                    marginTop: '18px',
                                                    color: '#4c4a55',
                                                    fontSize: '20px',
                                                    fontWeight: 'bold',
                                                }}>
                                                    {this.state.vol}
                                                </Row>
                                                <Row style={{
                                                    fontSize: '16px', color: '#000'
                                                }}>
                                                    {
                                                        this.state.volunteer.map((volunteer) => (
                                                            <Col span={20}>
                                                                <div style={{marginBottom: '5%'}}>
                                                                    <div style={{fontWeight: 'bold'}}>{volunteer.posisi} ({volunteer.tahunMulai} - {volunteer.tahunSelesai})</div>
                                                                    {volunteer.perusahaan}
                                                                    <br/>{volunteer.deskripsi}
                                                                </div>
                                                            </Col>
                                                        ))
                                                    }
                                                </Row>
                                                {/* achievement */}
                                                <Row style={{
                                                    marginTop: '18px',
                                                    color: '#4c4a55',
                                                    fontSize: '20px',
                                                    fontWeight: 'bold',
                                                }}>
                                                    {this.state.ach}
                                                </Row>
                                                <Row style={{
                                                    fontSize: '16px', color: '#000'
                                                }}>
                                                    {
                                                        this.state.achievement.map((achievement) => (
                                                            <div style={{paddingBottom: '4%'}}>
                                                                <div style={{fontWeight: 'bold'}}>{achievement.title} ({achievement.tahun})</div>
                                                                {achievement.institusi}
                                                                <br/>{achievement.deskripsi}
                                                            </div>
                                                        ))
                                                    }
                                                </Row>
                                            </Col>
                                        </Row>
                                    </div>
                                ) : <Polos email={localStorage.getItem('email')}/>}
                            </Layout>
                        </div>
                    </Row>
                </div>
            </div>
        );
    }
}
 
export default Preview;
import React, { Component } from 'react';
import 'antd/dist/antd.css';
import '../css/view_content.css';
import { Form, Input, Button, Space, Row, Col, Card } from 'antd';
import { DeleteFilled, PlusOutlined, SaveOutlined } from '@ant-design/icons';
import axios from 'axios';
import history from '../../history.js';

class Education extends Component {
    constructor(props){
        super(props);
        this.state = {
            education : []
        }
    }

    componentDidMount(){
        axios.get("https://sevvydb.herokuapp.com/database/profile/pendidikan/all", {
            params: {
                email: localStorage.getItem('email')
            }
        })
        .then(response => response.data)
        .then((data) => {
            this.setState({education: data})
        });
    }

    handleRemove(jenjang, id){
        const newPendidikan = [];
        for(var i=0; i <= this.state.education.length-1; i++){
            if(this.state.education[i].jenjang !== jenjang){
                newPendidikan.push(this.state.education[i])
            }
        }
        this.setState({
            education: newPendidikan
        });

        axios.delete("https://sevvydb.herokuapp.com/database/profile/pendidikan",{
            params: {
                id: id
            }
        }).then(res => res.data)
    };

    render() {
        const onFinish = values => {
            const a = values.pendidikan.length;
    
            for (var i=0; i<a; i++){
                const jenjang = values.pendidikan[i].jenjang;
                const institusi = values.pendidikan[i].institusi;
                const jurusan = values.pendidikan[i].jurusan;
                const tahunMulai = values.pendidikan[i].tahunMulai;
                const tahunSelesai = values.pendidikan[i].tahunSelesai;
    
                axios.post("https://sevvydb.herokuapp.com/database/profile/pendidikan/" + localStorage.getItem('email'),
                        {
                            jenjang: jenjang,
                            namaInstitusi: institusi,
                            jurusan: jurusan,
                            tahunMulai: tahunMulai,
                            tahunSelesai: tahunSelesai
                        },
                        { withCredentials: true }
                    )
                    .then(response => 
                            console.log(response.data)
                )
            }
        };

        return ( 
            <div>
                { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && localStorage.getItem('email') !== null ? (
                    <div>
                        <div>
                            <h2 className="title">Pendidikan</h2>     
                            {
                                this.state.education.map((education) => (
                                    <div className="inputArea">
                                        <Row style={{marginBottom: '2%'}}>
                                            <Col>
                                                <Card style={{
                                                    width: 700,
                                                    paddingLeft: 20,
                                                    paddingTop: 10,
                                                    paddingBottom: 10,
                                                    paddingRight: 20
                                                }}>
                                                    <Row>
                                                        <Col span={9}>
                                                            <div className="textField">
                                                                {education.jenjang}, {education.namaInstitusi}
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col span={9}>
                                                            <div className="text-ach">
                                                                {education.jurusan}
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col span={9}>
                                                            <div className="text-ach">
                                                                {education.tahunMulai} - {education.tahunSelesai}
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </Card>
                                            </Col>
                                            <Col>
                                                <DeleteFilled
                                                    className="delete"
                                                    style={{ margin: '0 8px', marginTop:'8px'}}
                                                    onClick={(e) => this.handleRemove(education.jenjang, education.id)}
                                                />
                                            </Col>
                                        </Row>
                                    </div>
                                ))
                            }
                        </div>
                        <Form name="dynamic_form_nest_item" onFinish={onFinish}>
                            <Form.List name="pendidikan">
                            {(fields, { add, remove }) => {
                                return (
                                    <div>
                                        {fields.map(field => (
                                        <Space key={field.key} style={{ display: 'flex', marginBottom: 5 }} align="start">
                                            <div className="inputArea">
                                                <Row>
                                                    <Col>
                                                        <Card style={{paddingLeft: 30, paddingTop: 30, marginBottom: 10}}>
                                                            <Row>
                                                                <Col span={10}>
                                                                    <div className="textArea">
                                                                        JENJANG
                                                                    </div>
                                                                    <Form.Item
                                                                    {...field}
                                                                    name={[field.name, 'jenjang']}
                                                                    fieldKey={[field.fieldKey, 'jenjang']}
                                                                    >
                                                                    <Input style={{width: '300px', marginRight: '46px'}} />
                                                                    </Form.Item>
                                                                </Col>
                                                                <Col span={10}>
                                                                    <div className="textArea">
                                                                        NAMA INSTITUSI
                                                                    </div>
                                                                    <Form.Item
                                                                    {...field}
                                                                    name={[field.name, 'institusi']}
                                                                    fieldKey={[field.fieldKey, 'institusi']}
                                                                    >
                                                                    <Input style={{width: '380px', marginRight: '46px'}} />
                                                                    </Form.Item>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col >
                                                                    <div className="textArea">
                                                                        JURUSAN
                                                                    </div>
                                                                    <Form.Item
                                                                    {...field}
                                                                    name={[field.name, 'jurusan']}
                                                                    fieldKey={[field.fieldKey, 'jurusan']}
                                                                    >
                                                                    <Input  style={{width: '380px', marginRight: '46px'}} />
                                                                    </Form.Item>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col >
                                                                    <div className="textArea">
                                                                        TAHUN MULAI
                                                                    </div>
                                                                        <Form.Item
                                                                        {...field}
                                                                        name={[field.name, 'tahunMulai']}
                                                                        fieldKey={[field.fieldKey, 'tahunMulai']}
                                                                    >
                                                                        <Input style={{width: '110px', marginRight: '46px'}} />
                                                                    </Form.Item>
                                                                </Col>
                                                                <Col >
                                                                    <div className="textArea">
                                                                        TAHUN SELESAI
                                                                    </div>
                                                                        <Form.Item
                                                                        {...field}
                                                                        name={[field.name, 'tahunSelesai']}
                                                                        fieldKey={[field.fieldKey, 'tahunSelesai']}
                                                                    >
                                                                        <Input style={{width: '110px', marginRight: '46px'}} />
                                                                    </Form.Item>
                                                                </Col>
                                                            </Row>
                                                        </Card>
                                                    </Col>
                                                    <Col>
                                                        {fields.length > 0 ? (
                                                            <DeleteFilled
                                                            className="delete"
                                                            style={{ margin: '0 8px', marginTop:'8px'}}
                                                            onClick={() => {
                                                                remove(field.name);
                                                            }}
                                                            />
                                                        ) : null}
                                                    </Col>
                                                </Row>
                                                </div>
                                            </Space>
                                            ))}
                                            <Form.Item>
                                            <Button
                                                type="dashed"
                                                onClick={() => {
                                                add();
                                                }}
                                                style={{ width: '40%' }}
                                            >
                                                <PlusOutlined /> Tambah Jenjang Pendidikan
                                            </Button>

                                            </Form.Item>
                                            <Form.Item>
                                            <Button type="primary" icon={<SaveOutlined />} size={"large"} className="save" htmlType="submit">
                                                Simpan
                                            </Button>
                                            </Form.Item>
                                        </div>
                                );
                            }}
                            </Form.List>
                        </Form>
                    </div>
                ) : history.push('/') }
            </div>
        );
    }
}
 
export default Education;
import React, {Component} from 'react';
import '../css/Sider.css';
import 'antd/dist/antd.css';
import { UserOutlined, BellOutlined, BookOutlined, 
    RocketOutlined, LikeOutlined, CarryOutOutlined, 
    DollarCircleOutlined, FilePdfOutlined } from '@ant-design/icons';
import { Menu, Layout } from "antd";

class SiderLayout extends Component {
  render() { 
    const styleDivider={
      height: '3px',
      backgroundColor: '#DAD7D7',
      width: '80%',
    }

    const { handleClick } = this.props;
    const Sider = () =>{
        return(
          <Menu className="edit" mode="inline" openKeys={"1"} defaultSelectedKeys={"1"}>
            <Menu.Item icon={<UserOutlined />} key="1" onClick={handleClick}>Informasi Diri</Menu.Item>
            <Menu.Item icon={<BellOutlined />} key="2" onClick={handleClick}>Situs Jaringan & Sosial Daring</Menu.Item>
            <Menu.Item icon={<BookOutlined />} key="3" onClick={handleClick}>Pendidikan</Menu.Item>
            <Menu.Item icon={<RocketOutlined />} key="4" onClick={handleClick}>Pencapaian</Menu.Item>
            <Menu.Item icon={<LikeOutlined />} key="5" onClick={handleClick}>Keterampilan</Menu.Item>
            <Menu.Item icon={<CarryOutOutlined />} key="6" onClick={handleClick}>Pengalaman Kerja</Menu.Item>
            <Menu.Item icon={<DollarCircleOutlined />} key="7" onClick={handleClick}>Kegiatan Sukarela</Menu.Item>
            <Menu.Divider style={styleDivider}/>
            <Menu.Item icon={<FilePdfOutlined />} key="8" onClick={handleClick}>Pratinjau</Menu.Item>
          </Menu>
        )
    }
    return ( 
      <div>
        <Layout.Sider>
         {Sider()}          
        </Layout.Sider>
      </div>
     );
  }
}
 
export default SiderLayout;
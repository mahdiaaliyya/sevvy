import React, {Component} from 'react';
import axios from 'axios';

import SignUp from './sign-up.jsx';

export default class Auth extends Component {
    constructor(props){
        super(props);
        this.handleSuccessfulAuth = this.handleSuccessfulAuth.bind(this);
        this.handleSuccessfulAuthLogin = this.handleSuccessfulAuthLogin.bind(this);
        this.handleLogoutClick = this.handleLogoutClick.bind(this);
    }

    handleSuccessfulAuth(data) {
        this.props.handleLogin(data);
        this.props.history.push("/buat-cv");
    }

    handleSuccessfulAuthLogin(data) {
        this.props.handleLogin(data);
        this.props.history.push("/dashboard");
    }

    handleLogoutClick() {
        axios
          .delete("https://sevvydb.herokuapp.com/", { withCredentials: true })
          .then(response => {
            this.props.handleLogout();
          })
    }

    render(){
        return(
            <div>
                <SignUp handleSuccessfulAuth={this.handleSuccessfulAuth}  handleSuccessfulAuthLogin={this.handleSuccessfulAuthLogin}/>
            </div>
        )
    }
}
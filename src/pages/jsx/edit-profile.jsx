import React, { useState } from 'react';
import { Layout} from 'antd';
import { Link } from "react-router-dom";
import Navbar from '../../components/jsx/NavigationBar';
import SiderLayout from '../../components/jsx/Sider';
import ProfileInfo from '../../components/jsx/edit_profile-info';
import Socmed from '../../components/jsx/edit_socmed';
import Education from '../../components/jsx/edit_education';
import Achievement from '../../components/jsx/edit_achievement';
import Skill from '../../components/jsx/edit_skill';
import Experience from '../../components/jsx/edit_experience';
import Volunteer from '../../components/jsx/edit_volunteer';
import Preview from '../../components/jsx/preview-page';

import Footer from '../../components/jsx/Footer';

const { Content } = Layout;


const EditProfile = (props) => {

    const components = {
        1: <Link to="" component={ProfileInfo} email={props.email} loggedInStatus={props.loggedInStatus}/>,
        2: <Link to="" component={Socmed} email={props.email} loggedInStatus={props.loggedInStatus}/>,
        3: <Link to="" component={Education} email={props.email} loggedInStatus={props.loggedInStatus}/>,
        4: <Link to="" component={Achievement} email={props.email} loggedInStatus={props.loggedInStatus}/>,
        5: <Link to="" component={Skill} email={props.email} loggedInStatus={props.loggedInStatus}/>,
        6: <Link to="" component={Experience} email={props.email} loggedInStatus={props.loggedInStatus}/>,
        7: <Link to="" component={Volunteer} email={props.email} loggedInStatus={props.loggedInStatus}/>,
        8: <Link to="" component={Preview}  email={props.email} loggedInStatus={props.loggedInStatus}/>,
    };
    
    const [render, updateRender] = useState(1);
    
    const handleMenuClick = menu => {
        updateRender(menu.key);
    };

        return ( 
            <div>
            <Navbar />
                <Layout
                    style={{ 
                        minHeight: "100vh",}}>
                    <SiderLayout handleClick={handleMenuClick}/>
                    <Content>{components[render]}</Content>
                </Layout>
                <Footer />
            </div>
        );
    
}
 
export default EditProfile;

import React, { Component } from 'react';
import { Layout, Collapse, Col, Button } from 'antd';
import { ArrowRightOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css';
import '../css/create-profile.css';

import history from '../../history.js';

import brandLogo from "../../images/brandLogo.png";

import ProfileInfo from '../../components/jsx/content_profile-info';
import Socmed from '../../components/jsx/content_socmed';
import Education from '../../components/jsx/content_education';
import Achievement from '../../components/jsx/content_achievement';
import Skill from '../../components/jsx/content_skill';
import Experience from '../../components/jsx/content_experience';
import Volunteer from '../../components/jsx/content_volunteer';

const { Panel } = Collapse;

class CreateProfile extends Component{
    constructor(props){
        super(props);
        this.setState = {
            email: ''
        }
    }

    handleSubmit() {
        this.props.history.push('/dashboard');
    }

    render(){
        return ( 
            <div>
                { this.props.loggedInStatus === 'LOGGED_IN' ? (
                    <Layout>
                        <Col xs={20} sm={20} md={6} lg={6} xl={5} xxl={4} className="menu">
                            <a href="/" style={{display: 'flex', alignItems: 'center'}}>
                                <img src={brandLogo} alt="Sevvy" style={{height: '60px'}}/>
                                <h1 className="logo" style={{margin: '0px'}}>sevvy</h1></a>
                        </Col>
                        <Collapse accordion>
                            <Panel header="Informasi Diri" key="1">
                                <ProfileInfo email={this.props.email} loggedInStatus={this.props.loggedInStatus}/>
                            </Panel>
                            <Panel header="Situs Jaringan & Sosial Daring" key="2">
                                <Socmed email={this.props.email} loggedInStatus={this.props.loggedInStatus}/>
                            </Panel>
                            <Panel header="Pendidikan" key="3">
                                <Education email={this.props.email} loggedInStatus={this.props.loggedInStatus}/>
                            </Panel>
                            <Panel header="Pencapaian" key="4">
                                <Achievement email={this.props.email} loggedInStatus={this.props.loggedInStatus}/>
                            </Panel>
                            <Panel header="Keterampilan" key="5">
                                <Skill email={this.props.email} loggedInStatus={this.props.loggedInStatus}/>
                            </Panel>
                            <Panel header="Pengalaman Kerja" key="6">
                                <Experience email={this.props.email} loggedInStatus={this.props.loggedInStatus}/>
                            </Panel>
                            <Panel header="Kegiatan Sukarela" key="7">
                                <Volunteer email={this.props.email} loggedInStatus={this.props.loggedInStatus}/>
                            </Panel>
                            
                        </Collapse>
                        <Button 
                            type="primary" 
                            shape="round" 
                            icon={<ArrowRightOutlined />} 
                            size={'large'} 
                            style={{
                                width: '120px',
                                left: '1200px'
                            }}
                            onClick={() => this.handleSubmit()}
                        >
                            Selesai
                        </Button>
                    </Layout>
                ) : history.push('/')};
            </div>
        );
    }
}

export default CreateProfile;
import React, {Component} from 'react';
import { PlusOutlined, EditFilled } from '@ant-design/icons';
import { Container, Row, Col} from 'react-bootstrap';
import { Space, Button, Layout, List } from 'antd';
import { DeleteFilled } from '@ant-design/icons';
import 'antd/dist/antd.css';
import '../css/Beranda.css';
import axios from 'axios';

import history from '../../history.js';

import Navbar from '../../components/jsx/NavigationBar.jsx';
import Footer from '../../components/jsx/Footer.jsx';

class Dashboard extends Component  {
    constructor(props){
        super(props)
        this.state = {
            localEmail: '',
            loggedInStatus: 'NOT_LOGGED_IN',
            profile: [],
            cv: []
        };
    }

    componentDidMount(){
        axios
        .get("https://sevvydb.herokuapp.com/database/profile/personal", {
            params: {
                email: localStorage.getItem('email')
            }
        })
        .then(response => {
            this.setState({profile: response.data});
            return axios.get("https://sevvydb.herokuapp.com/database/cv/all", {
                params: {
                    email: localStorage.getItem('email')
                }
            });
        })
        .then(response => {
            this.setState({cv: response.data});
        })
    }

    handleRemove(id){
        const newCV = [];
        for(var i=0; i <= this.state.cv.length-1; i++){
            if(this.state.cv[i].id !== id){
                newCV.push(this.state.cv[i])
            }
        }
        this.setState({
            cv: newCV
        });

        axios.delete("https://sevvydb.herokuapp.com/database/cv",{
            params: {
                id: id
            }
        }).then(res => res.data)
    };

    render() {
        return (  
            <div>
                { localStorage.getItem('loggedInStatus') === 'LOGGED_IN' && this.state.localEmail !== null ? (
                    <Layout>
                        <Navbar email={this.props.email}/>
                        <Container>
                            <Row>
                            <Col>
                                <div>
                                    <Row className="namePlaceholder">
                                        {this.state.profile.map((profile) => (
                                            <h1 className="namePlaceholder">{profile.firstName} {profile.lastName}</h1>
                                        ))}
                                    </Row>
                                    <br/>
                                    <Row className="edit-btn">
                                        <Space size={"middle"}>
                                            <Button title="create" type="primary" size={'large'} onClick={() => history.push('/buat-cv')}>
                                                <PlusOutlined /> Buat CV
                                            </Button>
                                            <Button title="edit" type="primary" size={'large'} onClick={() => history.push('/ubah-profil')}>
                                                <EditFilled />Ubah CV
                                            </Button>
                                        </Space>
                                    </Row>
                                </div>   
                                <div>
                                    <List
                                        size="large"
                                        style={{marginBottom: '10%', width: '60%', marginLeft: 'auto', marginRight: 'auto'}}
                                        header={<div className="myCV">CV Saya</div>}>
                                            {this.state.cv.map((cv) => (
                                                <List.Item style={{
                                                    fontSize: '18px',
                                                    textAlign: 'left'
                                                }}>
                                                    {cv.cvName}
                                                    <DeleteFilled
                                                        className="delete"
                                                        style={{ marginRight: '20px', marginLeft: '50px'}}
                                                        onClick={(e) => this.handleRemove(cv.id)}
                                                    />
                                                    <EditFilled
                                                        className="delete"
                                                        style={{ marginRight: '30px'}}
                                                        onClick={(e) => history.push('/ubah-profil')}
                                                    />
                                                </List.Item>
                                            ))
                                            }
                                    </List>
                                </div>
                            </Col>
                            </Row>
                        </Container>  
                        <Footer />  
                    </Layout>
                ): history.push('/')}
            </div>
        );
    }
}

export default Dashboard;
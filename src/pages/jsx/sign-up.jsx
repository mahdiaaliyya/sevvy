import React, {Component} from 'react';
import { Container, Jumbotron} from 'react-bootstrap';
import { Col, Input, Layout, Button, Row, Form } from 'antd';
import 'antd/dist/antd.css';
import '../css/sign-up.css';
import axios from 'axios';

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            loggedInStatus: '',
            users: []
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({
          [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        const { email, password } = this.state;
    
        axios
          .post(
            "https://sevvydb.herokuapp.com/database/register",
            {
                email: email,
                password: password
            },
            { withCredentials: true }
          )
          .then(response => {
              this.props.handleSuccessfulAuth(response.data);
          })
        event.preventDefault();
    }

    checkSignUpData(event){
        axios
            .get("https://sevvydb.herokuapp.com/database/users/all")
            .then(res => {
                var isExist = false;
                var emailInArray;
                
                const size = res.data.length;
                for( var i=0; i < size; i++){
                    emailInArray = res.data[i].email;
                    if( emailInArray === this.state.email){
                        isExist = true;
                    } 
                }

                if(isExist === true){
                    console.log("email already exist")
                } else {
                    localStorage.setItem('email', this.state.email)
                    this.handleSubmit(event);
                }
            })
    }    

    async checkLoginData(event){
        axios
            .get("https://sevvydb.herokuapp.com/database/users/all")
            .then(res => {
                var isExist = false;
                var emailInArray;
                var password;
                
                const size = res.data.length;
                for( var i=0; i < size; i++){
                    emailInArray = res.data[i].email;
                    password = res.data[i].password;
                    if( emailInArray === this.state.email && password === this.state.password){
                        isExist = true;
                    } 
                }

                if(isExist === true){
                    localStorage.setItem('email', this.state.email)
                    localStorage.setItem('loggedInStatus', 'LOGGED_IN')
                    this.handleLogin(event)
                } else {
                    console.log("user not found")
                }
            })
    }

    handleLogin(event) {
        const { email } = this.state;
    
        axios
          .get(
            "https://sevvydb.herokuapp.com/database/users",
            {
                params: {
                    email: email,
                }
            },
            { withCredentials: true }
          )
          .then(response => {
              this.props.handleSuccessfulAuthLogin(response.data);
          })
          
        event.preventDefault();

    }

    render(){
        return (  
            <Layout>
                <Container>
                    <div className="title-sign-up">
                    <img src={require('../../images/Logo-black.png')} alt='' style={{width: '277px', height: '87px', marginBottom: '5%'}}/>
                        <h1>Welcome to Sevvy</h1>
                        <h3>Create your CV!</h3>
                    </div>
                    <Row>
                        <Col span={12}>
                        <Jumbotron className="jumbotron-landing-page">
                            <Form>
                                <h4>Sign Up</h4>
                                <br/>
                                <Form.Item
                                    name="email"
                                    rules={[
                                        {
                                          type: 'email',
                                          message: 'The input is not valid email!',
                                        },
                                        {
                                          required: true,
                                          message: 'Please input your email!',
                                        },
                                    ]}    
                                >
                                    <Input 
                                        className="input-sign-up" 
                                        type="text" 
                                        name="email" 
                                        placeholder="Email" 
                                        value={this.state.email}
                                        onChange={e => this.handleChange(e)}/> 
                                </Form.Item>
                                <Form.Item
                                    name="password"  
                                    rules={[
                                        {
                                          required: true,
                                          message: 'Please input your password!',
                                        },
                                    ]}
                                    hasFeedback  
                                >
                                    <Input
                                        className="input-sign-up" 
                                        type="password" 
                                        name="password" 
                                        placeholder="Password" 
                                        value={this.state.password}
                                        onChange={e => this.handleChange(e)}/>
                                </Form.Item>
                                <Form.Item
                                    name="confirm"  
                                    dependencies={['password']}
                                    hasFeedback
                                    rules={[
                                    {
                                        required: true,
                                        message: 'Please confirm your password!',
                                    },
                                    ({ getFieldValue }) => ({
                                        validator(rules, value) {
                                        if (!value || getFieldValue('password') === value) {
                                            return Promise.resolve();
                                        }

                                        return Promise.reject('The two passwords that you entered do not match!');
                                        },
                                    }),
                                    ]}
                                >
                                    <Input
                                        className="input-sign-up" 
                                        type="password" 
                                        name="password" 
                                        placeholder="Confirm your password" 
                                        value={this.state.password}
                                        onChange={e => this.handleChange(e)}/>
                                </Form.Item>
                                <Row>
                                    <Button 
                                        type="primary"  
                                        size={"large"} 
                                        className="sign-up-btn" 
                                        onClick={(e) => this.checkSignUpData(e)}>
                                        Sign Up
                                    </Button>
                                </Row>
                            </Form>
                        </Jumbotron>
                        </Col>
                        <Col span={12}>
                        <Jumbotron className="jumbotron-landing-page">
                            <div>
                                <h4>Sign In</h4>
                                <br/>
                                <Form.Item
                                    name="email"
                                    rules={[
                                        {
                                          type: 'email',
                                          message: 'The input is not valid email!',
                                        },
                                        {
                                          required: true,
                                          message: 'Please input your email!',
                                        },
                                    ]}    
                                >
                                    <Input 
                                        className="input-sign-up" 
                                        type="text" 
                                        name="email" 
                                        placeholder="Email" 
                                        value={this.state.email}
                                        onChange={e => this.handleChange(e)}/> 
                                </Form.Item>
                                <Form.Item
                                    name="password"  
                                >
                                    <Input
                                        className="input-sign-up" 
                                        type="password" 
                                        name="password" 
                                        placeholder="Password" 
                                        value={this.state.password}
                                        onChange={e => this.handleChange(e)}/>
                                </Form.Item>
                                <Row>
                                <Button 
                                    type="primary"  
                                    size={"large"} 
                                    className="sign-up-btn" 
                                    onClick={(e) => this.checkLoginData(e)}>
                                    Sign In
                                </Button>
                            </Row>
                            </div>
                        </Jumbotron>
                        </Col>
                    </Row>
                </Container>
            </Layout>
        );
    }
}

export default SignUp;

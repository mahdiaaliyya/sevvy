import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Content from '../jsx/sign-up.jsx';
import axios from 'axios';

import MockAdapter from 'axios-mock-adapter';

let mock;

configure({ adapter: new Adapter() });

describe('Sign Up', () => {
    it('renders without crashing', () =>{
      shallow(<Content />);
    })

    it("should call preventDefault", () => {
      let wrapper;

      const mockPreventDefault = jest.fn();
      const mockEvent = {
        preventDefault: mockPreventDefault
      };
      expect(wrapper).toMatchSnapshot();
    });

    it('componentDidMount',  () => {
      beforeEach(() => {
        mock = new MockAdapter(axios);
        mock.onGet('/sevvydb.herokuapp.com/database/users/all').reply(200, {
          users: [
            { id: 1, name: 'John Smith' }
          ]
        });
    });
    
      afterEach(() => {
        mock.restore();
      });
    });

});
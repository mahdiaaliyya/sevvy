import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Beranda from '../../pages/jsx/Beranda.jsx';
import { Button } from 'antd';
import { render, fireEvent } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

configure({ adapter: new Adapter() });


beforeAll(() => {
    jest.mock('react-router-dom', () => ({
        ...jest.requireActual('react-router-dom'),
        useHistory: () => ({
        push: mockHistoryPush,
        }),
    }));

    const mockHistoryPush = jest.fn();

    global.fetch = jest.fn();
});

let wrapper;

beforeEach(() => {
   wrapper = shallow(<Beranda />, { disableLifecycleMethods: true });
});

afterEach(() => {
    wrapper.unmount();
 });

describe('Beranda', () => {
    it('renders without crashing', () =>{
        shallow(<Beranda />);
    })

    it('onclick button works', () => {
        const handleButtonClick = jest.fn();
        const wrapper = shallow(<Button />);
        wrapper.find('button').simulate('click');
        expect(handleButtonClick.mock.calls.length).toEqual(0);
    })

    
    it('onClick called', () => {
        const mockOnClick = jest.fn();
        const wrapper = shallow(<Button onClick={mockOnClick} />)
        wrapper.find('button').simulate('click', 'junk')
        expect(mockOnClick.mock.calls.length).toEqual(1)
    })

    // it('mock history push buat-cv', () => {
    //     const { getByTitle } = render(
    //         <MemoryRouter>
    //           <Button title="create" type="primary" size={'large'}/>
    //         </MemoryRouter>,
    //       );
    //     fireEvent.click(getByTitle('create'));
    //     expect(jest.fn()).toMatchSnapshot('/buat-cv');
    // })

    // it('mock history push ubah-profile', () => {
    //     const { getByTitle } = render(
    //         <MemoryRouter>
    //           <Button title="edit" type="primary" size={'large'}/>
    //         </MemoryRouter>,
    //       );
    //     fireEvent.click(getByTitle('edit'));
    //     expect(jest.fn()).toMatchSnapshot('/ubah-profil');
    // })
});
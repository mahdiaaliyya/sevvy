import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Layout, Menu } from 'antd';
import SiderLayout from '../components/jsx/Sider.jsx'

configure({ adapter: new Adapter() });

describe('Sider', () => {
    it('renders without crashing', () =>{
      shallow(<SiderLayout />);
    })

    it('renders sider layout', () => {
      const wrapper = mount(<Layout.Sider />);
      expect(wrapper).toMatchSnapshot();
    })

    it('renders menu', () => {
      const wrapper = mount(<Menu />);
      expect(wrapper).toMatchSnapshot();
    })

    it('renders informasi diri tab', () => {
      const wrapper = shallow(<Menu.Item>Informasi Diri</Menu.Item>);
      expect(wrapper).toMatchSnapshot();
    })

    it('renders Situs Jaringan & Sosial Daring tab', () => {
      const wrapper = shallow(<Menu.Item>Situs Jaringan & Sosial Daring</Menu.Item>);
      expect(wrapper).toMatchSnapshot();
    })

    it('renders Pendidikan tab', () => {
      const wrapper = shallow(<Menu.Item>Pendidikan</Menu.Item>);
      expect(wrapper).toMatchSnapshot();
    })

    it('renders Pencapaian tab', () => {
      const wrapper = shallow(<Menu.Item>Pencapaian</Menu.Item>);
      expect(wrapper).toMatchSnapshot();
    })

    it('renders Keterampilan tab', () => {
      const wrapper = shallow(<Menu.Item>Keterampilan</Menu.Item>);
      expect(wrapper).toMatchSnapshot();
    })

    it('renders Pengalaman Kerja tab', () => {
      const wrapper = shallow(<Menu.Item>Pengalaman Kerja</Menu.Item>);
      expect(wrapper).toMatchSnapshot();
    })

    it('renders Kegiatan Sukarela tab', () => {
      const wrapper = shallow(<Menu.Item>Kegiatan Sukarela</Menu.Item>);
      expect(wrapper).toMatchSnapshot();
    })

    it('renders Pratinjau tab', () => {
      const wrapper = shallow(<Menu.Item>Pratinjau</Menu.Item>);
      expect(wrapper).toMatchSnapshot();
    })
});
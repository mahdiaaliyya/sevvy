import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import NavigationBar from '../components/jsx/NavigationBar.jsx';
import { Menu } from "antd";
import { render, fireEvent } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

configure({ adapter: new Adapter() });

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useHistory: () => ({
      push: mockHistoryPush,
    }),
}));

Object.defineProperty(window, 'matchMedia', {
    writable: true,
    value: jest.fn().mockImplementation(query => ({
      matches: false,
      media: query,
      onchange: null,
      addListener: jest.fn(), // deprecated
      removeListener: jest.fn(), // deprecated
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn(),
    })),
  });

const mockHistoryPush = jest.fn();

describe('NavigationBar', () => {

    it('renders logo from right source', () => {
        const wrapper= shallow(<NavigationBar />);
        expect(wrapper.find('img').props().src).toEqual('brandLogo.png'); 
    })

    it('renders brand with logo', () => {
        const wrapper = shallow(<h1>sevvy</h1>);
        expect(wrapper).toMatchSnapshot();
    })

    it('renders Beranda tab', () => {
        const wrapper = shallow(<Menu.Item>Beranda</Menu.Item>);
        expect(wrapper).toMatchSnapshot();
    })

    it('renders Profil Saya tab', () => {
        const wrapper = shallow(<Menu.Item>Profil Saya</Menu.Item>);
        expect(wrapper).toMatchSnapshot();
    })

    it('renders Akun tab', () => {
        const wrapper = shallow(<Menu.Item>Akun</Menu.Item>);
        expect(wrapper).toMatchSnapshot();
    })

    it('renders Keluar tab', () => {
        const wrapper = shallow(<Menu.Item>Keluar</Menu.Item>);
        expect(wrapper).toMatchSnapshot();
    })

    it('mock history push dashboard', () => {
        const { getByTitle } = render(
            <MemoryRouter>
              <NavigationBar />
            </MemoryRouter>,
          );
        fireEvent.click(getByTitle('beranda'));
        expect(jest.fn()).toMatchSnapshot('/dashboard');
    })

    it('mock history push profil', () => {
        const { getByTitle } = render(
            <MemoryRouter>
              <NavigationBar />
            </MemoryRouter>,
          );
        fireEvent.click(getByTitle('profil'));
        expect(jest.fn()).toMatchSnapshot('/profil');
    })

    it('mock history push keluar', () => {
        const { getByTitle } = render(
            <MemoryRouter>
              <NavigationBar />
            </MemoryRouter>,
          );
        fireEvent.click(getByTitle('keluar'));
        expect(jest.fn()).toMatchSnapshot('/');
    })
});
import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Content from '../components/jsx/content_volunteer.jsx'
import { Input } from 'antd';

configure({ adapter: new Adapter() });

describe('Sider', () => {
    it('renders without crashing', () =>{
      shallow(<Content />);
    })

    it('renders title', () => {
        const wrapper = shallow(<h2>Pencapaian</h2>);
        expect(wrapper).toMatchSnapshot();
    })

    it('uses the right class for layout', () => {
      const wrapper = shallow(<div className="site-layout-background" />);
      expect(wrapper.is('.site-layout-background')).toEqual(true);
    })

    it('uses the right class for input area', () => {
        const wrapper = shallow(<div className="inputArea"/>);
        expect(wrapper.is('.inputArea')).toEqual(true);
      })

    it('uses the right class for text area', () => {
      const wrapper = shallow(<div className="textArea"/>);
      expect(wrapper.is('.textArea')).toEqual(true);
    })

    it('input area can be used', () => {
        const wrapper = mount(<Input />);
        expect(wrapper).toMatchSnapshot();
    })
});
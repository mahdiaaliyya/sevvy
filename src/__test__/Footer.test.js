import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Footer from '../components/jsx/Footer.jsx';

configure({ adapter: new Adapter() });

describe('Footer displayed text', () => {
   it('renders created-by text', () => {
        const wrapper = mount(<Footer>Sevvy © 2020 Created by Kelompok 11</ Footer>);
        expect(wrapper.text()).toEqual('Sevvy © 2020 Created by Kelompok 11');
    });
});
import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Preview from '../components/jsx/preview-page.jsx';
import { Button } from 'antd';

configure({ adapter: new Adapter() });

describe('Preview', () => {
    it('renders without crashing', () =>{
      shallow(<Preview />);
    })
    
    it('onclick button works', () => {
        const handleButtonClick = jest.fn();
        const wrapper = shallow(<Button />);
        wrapper.find('button').simulate('click');
        expect(handleButtonClick.mock.calls.length).toEqual(0);
    })
});
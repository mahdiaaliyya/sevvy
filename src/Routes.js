import React, { Component } from "react";
import { Router, Switch, Route } from "react-router-dom";
import Beranda from './pages/jsx/Beranda.jsx';
import IsiProfil from './pages/jsx/create-profile.jsx';
import UbahProfil from './pages/jsx/edit-profile.jsx';
import Profil from './pages/jsx/view-profile.jsx';
// import SignUp from './pages/jsx/sign-up.jsx';
import Auth from './pages/jsx/auth.jsx';
import history from './history';
import axios from 'axios';

export default class Routes extends Component {
    constructor() {
        super();

        this.state = {
            loggedInStatus: "NOT_LOGGED_IN",
            email: ''
        }

        this.handleLogin = this.handleLogin.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
    }

    checkLoginStatus() {
        axios
            .get("https://sevvydb.herokuapp.com/database/users/all", { withCredentials: true })
            .then(response => {
                if (
                    response.data.logged_in &&
                    this.state.loggedInStatus === "NOT_LOGGED_IN"
                ) { 
                    this.setState({
                    loggedInStatus: "LOGGED_IN",
                    email: response.data.email
                    });
                } else if (
                    !response.data.logged_in &
                    (this.state.loggedInStatus === "LOGGED_IN")
                ) {
                    this.setState({
                    loggedInStatus: "NOT_LOGGED_IN",
                    user: {}
                    });
                }
            })
    }
    
    componentDidMount() {
        this.checkLoginStatus();
    }
    
    handleLogout() {
        this.setState({
          loggedInStatus: "NOT_LOGGED_IN",
          email: ''
        });
    }
    
    handleLogin(data) {
        this.setState({
            loggedInStatus: "LOGGED_IN",
            email: data.email,
        });
    }

    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route 
                        path={"/"} 
                        exact
                        render={props => (
                            <Auth 
                            {...props} 
                            handleLogin={this.handleLogin}
                            handleLogout={this.handleLogout}
                            loggedInStatus={this.state.loggedInStatus}/> 
                            )}
                    />

                    <Route path={"/dashboard"} 
                        render={props => (
                            <Beranda {...props} 
                            handleLogin={this.handleLogin}
                            handleLogout={this.handleLogout}
                            email={this.state.email}
                            loggedInStatus={this.state.loggedInStatus} />
                    )} />
                    <Route 
                        path={"/buat-cv"} 
                        exact
                        render={props => (
                            <IsiProfil {...props} 
                            handleLogin={this.handleLogin}
                            handleLogout={this.handleLogout}
                            email={this.state.email}
                            loggedInStatus={this.state.loggedInStatus} /> )}
                    />
                    <Route path={"/ubah-profil"} 
                        render={props => (
                            <UbahProfil {...props} 
                            handleLogin={this.handleLogin}
                            handleLogout={this.handleLogout}
                            email={this.state.email}
                            loggedInStatus={this.state.loggedInStatus} /> )}
                    />
                    <Route path={"/profil"} 
                    render={props => (
                        <Profil {...props} 
                        handleLogin={this.handleLogin}
                        handleLogout={this.handleLogout}
                        email={this.state.email}
                        loggedInStatus={this.state.loggedInStatus} /> )}
                    />
                </Switch>
            </Router>
        )
    }
}